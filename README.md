Introduction
============

This is the (new) home of PyBox, a user-level framework for monitoring processes. 

PyBox consists of

* a module ([PyBox.dll](https://bitbucket.org/daniel_plohmann/pybox/src/c6f0d54b9906977231402e83e887bab0010c22f1/DLL/PyBox.cpp?at=master)), which is being injected into a target process, carrying a Python interpreter
* an [API](https://bitbucket.org/daniel_plohmann/pybox/src/c6f0d54b9906977231402e83e887bab0010c22f1/src/pybox/?at=master) providing various tools for logging and analysis of activity as well as full manipulation of processes. 

This rootkit-like approach allows close monitoring of behavior, thus helping to understand functionality of programs.
Wiki Overview

* The PyBox original idea is explained [here](https://eldorado.tu-dortmund.de/bitstream/2003/27336/1/BookOfAbstracts_Spring5_2010.pdf)
* The requirements for setting up your own PyBox are explained [here](https://bitbucket.org/daniel_plohmann/pybox/src/676d893a287fcdfd7dd18323341f313f81c1bea5/Requirements.md?at=master)
* Check also out this [blog post](http://pnx-tf.blogspot.de/2014/01/pybox-relaunch.html) on the internals of PyBox
